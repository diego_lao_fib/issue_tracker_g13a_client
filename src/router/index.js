import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Issues from '@/components/Issues'
import Show from '@/components/Show'
import New from '@/components/New'
import Edit from '@/components/Edit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/issues',
      name: 'Issues',
      component: Issues
    },
    {
      path: '/issues/new',
      name: 'New',
      component: New
    },
    {
      path: '/issues/:id',
      name: 'Show',
      component: Show
    },
    {
      path: '/issues/:id/edit',
      name: 'Edit',
      component: Edit
    }
  ]
})
